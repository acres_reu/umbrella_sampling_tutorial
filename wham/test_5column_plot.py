import numpy as np
#import pandas as pd
import matplotlib.pyplot as plt
import sys

wham_output = sys.argv[1]
n_frames = int(sys.argv[2])

def get_fe_error(input_file):
    data = np.loadtxt(input_file)

    FE_vals = []
    error = []
    coor = []
    for index,value in enumerate(data):
        coor.append(value[0])
        FE_vals.append(value[1])
        error.append(float(value[2]))
    
    return FE_vals, error, coor

def VLJ(r,epsilon_kcal):
    epsilon_kj = epsilon_kcal*4.184
    sigma = 0.3350 # nanometers                                                                                         
    sigma_o_r = sigma/r
    sigma_o_r6 = (sigma_o_r)**6
    return 4*epsilon_kj*(sigma_o_r6*(sigma_o_r6-1))

def Fanalytic(r,beta,epsilon):
    return -(2*np.log(r)-beta*VLJ(r,epsilon))/beta

T = 300 #K                                                                                                              
Kb = 0.008314 #(kJ/mol)/K                                                                                               
beta = 1/(T*Kb)

box = 4.31 #nm  
epsilon = 5

rs = np.linspace(0.315,2,1000)
Ftarget = Fanalytic(rs,beta,epsilon)
Ftarget -= Ftarget.min()

FE_75r1, err_75r1, coor_75r1 = get_fe_error(wham_output)

FE_kJ = []
for i in FE_75r1:
    if i == 'inf':
        FE_kJ.append(i)
    else:
        FE_kJ.append(i*4.184)

# need wham data in kJ!
if n_frames == 75:
    plt.errorbar(coor_75r1, FE_kJ, yerr=err_75r1, label='Eps5 75W')
if n_frames == 113:
    plt.errorbar(coor_75r1, FE_kJ, yerr=err_75r1, label='Eps5 113W')
plt.plot(rs, Ftarget, color='k', linestyle='--', label='Target')
plt.ylabel('FE (kJ/mol)')
plt.xlabel('Coordinate (nm)')
plt.legend()
if n_frames == 75:
    plt.title('Eps5, W75')
if n_frames == 113:
    plt.title('Eps5, W113')
plt.xticks(coor_75r1[::35])
plt.show()


            
