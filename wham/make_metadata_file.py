# coms for traj0 win_min
# this should be 75 lines, only change is com0.dat - com74.dat and pos of minimum!
# com0.dat 0.315 478.0115 10 
import sys

n_frames = int(sys.argv[1])

if n_frames == 75:
    d0_numbers = [0.315 + 15*i*0.0015 for i in range(75)]
if n_frames == 113:
     d0_numbers = [0.315 + 10*i*0.0015 for i in range(113)]

for i in range(n_frames):
    print(f'com{i}.dat {d0_numbers[i]} 478.0115 10')

