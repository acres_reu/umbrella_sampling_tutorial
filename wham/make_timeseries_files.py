import numpy as np
import sys

dists_file = sys.argv[1]
n_frames= int(sys.argv[2])

dist_list = np.loadtxt(dists_file)
dists = np.array(dist_list)

for i in range(n_frames):
    tmp = open(f'com{i}.dat','w+')
    for j in range(len(dists[i])):
        tmp.write(f'{j} {dists[i][j]} \n')
    tmp.close()
