import sys
from copy import copy
import os
import os.path as osp
import pickle
import random

import numpy as np

from simtk.openmm.app import *
from simtk.openmm import *
from simtk.unit import *

import simtk.openmm as omm
import simtk.unit as unit

from testsystems import LennardJonesPair
import mdtraj as mdj
from wepy.util.mdtraj import mdtraj_to_json_topology

from wepy.sim_manager import Manager

from wepy.resampling.resamplers.resampler import NoResampler
from wepy.walker import Walker
from wepy.runners.openmm import OpenMMRunner, OpenMMState, OpenMMGPUWorker
from wepy.runners.openmm import UNIT_NAMES, GET_STATE_KWARG_DEFAULTS
from wepy.work_mapper.mapper import Mapper
from wepy.work_mapper.mapper import WorkerMapper

from wepy.reporter.hdf5 import WepyHDF5Reporter
import logging

# set logging threshold
logging.basicConfig(level=logging.INFO,format='%(asctime)s %(message)s')

# Platform used for OpenMM which uses different hardware computation
# kernels. Options are: Reference, CPU, OpenCL, CUDA.
PLATFORM = 'Reference'

# Monte Carlo Barostat
PRESSURE = 1.0*unit.atmosphere
TEMPERATURE = 300.0*unit.kelvin
FRICTION_COEFFICIENT = 1/unit.picosecond
STEP_SIZE = 0.002*unit.picoseconds

# the maximum weight allowed for a walker
PMAX = 0.5
# the minimum weight allowed for a walker
PMIN = 1e-100

# reporting parameters

# these are the properties of the states (i.e. from OpenMM) which will
# be saved into the HDF5
SAVE_FIELDS = ('positions', 'box_vectors', 'velocities')
# these are the names of the units which will be stored with each field in the HDF5
UNITS = UNIT_NAMES
ALL_ATOMS_SAVE_FREQ = 5

## INPUTS/OUTPUTS

#Read the inputs
if sys.argv[1] == "-h" or sys.argv[1] == "--help":
    print("arguments: n_cycles, n_steps, epsilon, frame number, number of total frames, run number")
else:
    n_cycles = int(sys.argv[1])
    n_steps = int(sys.argv[2])
    epsilon = int(sys.argv[3])
    # frame number is the window of interest, it sets the force d0 value and the initial positions
    # this is any number between 0 and 74 for 75 starting windows
    frame_number = int(sys.argv[4])
    # n_frames is the total number of windows (either 75 or 113)
    n_frames = int(sys.argv[5])
    run = int(sys.argv[6])

windows = n_frames
# the inputs directory
inputs_dir = osp.realpath('./inputs')
# the outputs path
outputs_dir = osp.realpath(f'./outputs/eps{epsilon}/windows{n_frames}/run{run}')
# make the outputs dir if it doesn't exist
os.makedirs(outputs_dir, exist_ok=True)

n_walkers = 1
hdf5_filename = f'US_frame{frame_number}.wepy.h5'

# inputs filenames
json_top_filename = "pair.top.json"

# normalize the input paths
json_top_path = osp.join(inputs_dir, json_top_filename)

omm_states = []
get_state_kwargs = dict(GET_STATE_KWARG_DEFAULTS)
pdb_traj = mdj.load_pdb('inputs/dummy_top.pdb')

# this dcd has the starting positions for a 75 or 113 window simulation
# there are init positions for 75 and 113 windows and for epsilons of 5 and 20 kcal/mol for each
dcd = mdj.load_dcd(f'inputs/eps{epsilon}_{n_frames}frames.dcd', top=pdb_traj.topology)

# initiate the test system
test_sys = LennardJonesPair(epsilon=epsilon*unit.kilocalories_per_mole)

# change the box size
# Vectors set in Vec3. unit=nanometers
test_sys.system.setDefaultPeriodicBoxVectors([4.31,0,0],[0,4.31,0],[0,0,4.31])
system = test_sys.system
omm_topology = test_sys.topology
mdj_top = mdj.Topology.from_openmm(omm_topology)
json_top = mdtraj_to_json_topology(mdj_top)

# make the integrator and force, set positions
integrator = omm.LangevinIntegrator(TEMPERATURE, FRICTION_COEFFICIENT, STEP_SIZE)
if windows == 75:
    d0_numbers = [0.315 + 15*i*0.0015 for i in range(75)]
if windows == 113:
    d0_numers = [0.315 + 10*i*0.0015 for i in range(113)]

# get the d0 value for the frame you selected here
d0_val = d0_numbers[frame_number]
# build the harmonic force
total_lj = CustomBondForce("0.5*k*(r-r0)^2")
total_lj.addPerBondParameter('k')
total_lj.addPerBondParameter('r0')
# param [0] is the spring constant, param [1] is the init distance (nm)
# set the d0 value here
total_lj.addBond(0, 1, [2000, d0_val*unit.nanometer])
system.addForce(total_lj)

# make a context and set the positions
context = omm.Context(test_sys.system, integrator)

# positions are set here from the dcd with the pos relevent to your frame number
test_sys.positions = dcd.xyz[frame_number]*unit.nanometers
context.setPositions(test_sys.positions)

omm_states.append(context.getState(**get_state_kwargs))

omm_topology = test_sys.topology
mdj_top = mdj.Topology.from_openmm(omm_topology)
runner = OpenMMRunner(system, omm_topology, integrator, platform=PLATFORM, enforce_box=True)
                
# get the data from this context so we have a state to start the simulation with
init_state = OpenMMState(omm_states[0])

## Resampler
resampler = NoResampler()

## Reporters
# make a dictionary of units for adding to the HDF5
units = dict(UNIT_NAMES)

mapper = Mapper()

## Run the simulation

if __name__ == "__main__":

        
        # normalize the output paths
        hdf5_path = osp.join(outputs_dir, hdf5_filename)


        print("Number of steps: {}".format(n_steps))
        print("Number of cycles: {}".format(n_cycles))
        # # create the initial walkers
        init_weight = 1.0 / n_walkers
        init_walkers = [Walker(OpenMMState(omm_states[i]), init_weight) for i in range(n_walkers)]


        hdf5_reporter = WepyHDF5Reporter(file_path=hdf5_path, mode='w',
                                         # save_fields set to None saves everything
                                         save_fields=None,
                                         resampler=resampler,
                                         boundary_conditions=None,
                                         topology=json_top,
                                         units=units)
        reporters = [hdf5_reporter]

        sim_manager = Manager(init_walkers,
                              runner=runner,
                              resampler=resampler,
                              boundary_conditions=None,
                              work_mapper=mapper,
                              reporters=reporters)

        # make a number of steps for each cycle. In principle it could be
        # different each cycle
        steps = [n_steps for i in range(n_cycles)]

       # actually run the simulation
        print("Starting run: {}".format(0))
        sim_manager.run_simulation(n_cycles, steps)
        print("Finished run: {}".format(0))


        print("Finished first file")
