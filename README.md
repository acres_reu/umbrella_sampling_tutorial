## About this software
This repo contains the code and information necessary to run and analyze basic umbrella sampling simulations with the wepy software package originally designed for weighted ensemble simulations. Umbrella sampling is a useful counterpart to weighted ensemble because it is more efficient at calculating thermodynamic quantities, such as the free energy surface. It is a biased molecular dynamic simulation based on a collective variable, which in this case is the center-of-mass to center-of-mass distance. The simulations in this tutorial use a Lennard-Jones pair, which is a slightly modified version of the LJ pair available through OpenMMTools. This tutorial generates an individual wepy hdf5 file for each simulation umbrella. 

___

## Authors
Emma Fink(1), Nicole Roussey(2), & Alex Dickson(2)(3)

(1). The Department of Biochemistry, Providence College, Providence, Rhode Island

(2). The Department of Biochemistry and Molecular Biology, Michigan State University, East Lansing, Michigan

(3). The Department of Computational Mathematics, Science, & Engineering, Michigan State University, East Lansing, Michigan

---

## Installation and Requirements

First clone the git repo.

**git clone https://gitlab.com/acres_reu/umbrella_sampling_tutorial.git**  
(if you have never used git before, your computer may ask to install developer tools, click "install")  

The software can be installed with pip. This may require updating pip to the latest version with **pip install --upgrade pip**. The minimum python requirement is 3.6.

To install wepy,  
**pip install wepy**  
  
To install OpenMM,  
**conda install -c omnia openmm**  
If downloading OpenMM is a problem, make sure the correct version of Python is loaded    

To install mdtraj,  
**conda install -c conda-forge mdtraj**  

To make sure wepy was successfully installed,  
**wepy --help**  

To install matplotlib,  
**conda install -c conda-forge matplotlib**  

To install WHAM,  
A folder is included in the umbrella_sampling_tutorial directory called "wham".
The default units are kcal/mol (this can be altered by following directions in the "doc" directory if you choose). Addition of a PATH to the wham directory may also be necessary.    
To complete installation, start in the umbrella_sampling_tutorial directory and type the following 4 commands:  
1. **cd wham**
2. **cd wham** 
3. **make clean**
4. **make**    
---

## Contents of the Repository
In the umbrella_sampling_tutorial directory you will find 7 items:
1. README  
2. inputs
3. testsystems.py
4. launch_sims.sh
5. lj_umbrella75.py
6. analysis  
7. wham

##### Inputs
A directory which contains the PDB file (for 3D structural information) and two .dcd files (with trajectory data) for either 75 or 113 windows

##### testsystems.py
This module provides functions for building a number of test systems of varying complexity, in our case we will just be importing the lennard-jones pair

##### launch_sims.sh  
This script starts a group of simulations (either 75 or 113) with different starting positions using lj_umbrella.py. When this script is started, information about the simulation will appear on the screen and it could run anywhere from a few minutes to a few hours depending on how many cycles/steps/windows are included.

##### lj_umbrella75.py
This script contains the actual simulation instructions. It creates an output directory and writes the simulation data to it.  The output path is defined with the line: outputs_dir = osp.realpath(f'./outputs/eps{epsilon}/windows{n_frames}/run{run}')    
Six parameters are necessary to call lj_umbrella.py in launch_sims:
1. number of cycles (500)
2. number of steps (10000)
3. epsilon (5)
4. window number ($i) - int, the variable, ($i in launch_sims), selects the window to be simulated - selects both the starting positions and the target interactomic distance for the applied force
5. number of total frames (choose between two options: 75 or 113)
6. run number (increment the number each time a new "launch_sims.sh" call is made so that data is not rewritten)

##### analysis 
A directory which contains two python scripts:
1. COMtoCOM_analysis.py: this script creates three textfiles (bins, x-values, distances) which are necessary for building a COM-to-COM histogram/distribution and performing WHAM, need to pass in 1) number of frames and 2) run number
2. plot_COM_histogram.py: this script plots the histogram, need to pass in 1) number of frames and 2) run number  

##### wham
A directory which contains several directories and files which allow WHAM to run.  The three things we want to focus on are the three ".py" files. 
1. make_timeseries_files.py : creates a time series file for each window run (com0.dat - com74.dat) and creates a new directory for the files to be located. Requires two parameters: 1) distances.txt 2) the total number of frames 
2. make_metadata_file.py : Takes the information from the time series files and writes it to a textfile. Requires one parameter: 1) total number of frames 
3. test_5column_plot.py : plots the WHAM output for a LJ pair simulation.  Requires two parameters: 1) textfile and 2) total number of frames  

---

## Example testing  
##### Running US Simulations with Wepy
Before running - determine desired number of steps/cycle, number of cycles per window, and number of windows (75 or 113). Modify launch_sims.sh if necessary    
Step 1: Navigate to the "umbrella_sampling_tutorial" directory and type **bash launch_sims.sh** (The simulation is run and the output is saved)  
Step 2: After running the simulation, a directory called "outputs" should appear. To check that the simulation ran successfully, follow the path /outputs/eps5/windows75/run1/ (75 or 113 wepy.h5 files should appear in this directory)  
Step 3: Go back to "umbrella_sampling_tutorial" directory and enter the "analysis" directory  
Step 4: Type **python COMtoCOM_analysis.py 75 1** (creates 3 textfiles: xvals, bins, and distance, which should appear in the analysis directory)   

##### Histogram Plot  
Step 5: Type **python plot_COM_histogram.py 75 1** (plots histogram of simulation in pop-up window)  

##### WHAM  
Step 6: Move the distances file over to the wham directory by typing **mv distances_eps5_w75_run1.txt ../wham**  
Step 7: Navigate to "wham" directory (cd ../wham)    
Step 8: Type **python make_timeseries_files.py distances_eps5_w75_run1.txt 75** (75 time series files should appear in the wham directory com0.dat- com74.dat)  
Step 9: Type **python make_metadata_file.py > metadata.txt 75**  (prints the information from the com0.dat file to a textfile)  
Step 10: Type **wham 0.15 2.2 200 0.000001 300 0 metadata.txt test_outfile.txt 10 1234** (performs WHAM, and prints output to test_outfile.txt)
Note; WHAM inputs are hist_min hist_max num_bins convergence_tolerance temperature numpad metadata outfile_name MC_trials rand_seed (see WHAM documentation for details)    
Step 11: Type **python test_5column_plot.py test_outfile.txt 75** (plots the lennard jones free energy surface)

##### To Reset
* Remove com0.dat files in "wham" directory (type rm *.dat)
* When running with a different epsilon value, search each file for the previous epsilon value and replace
* Rename the output files for other rounds of the simulation (test_outfile.txt, metadata.txt)
* Change path for the output folder if changing windows or epsilon value

---

## References

##### Software Packages

'[Wepy](https://github.com/ADicksonLab/wepy)'

'[OpenMM](http://openmm.org)'

'[mdtraj](http://mdtraj.org/1.9.3/)'

'[NumPy](https://numpy.org)'

'[matplotlib](https://www.h5py.org)'

'[h5py](https://www.h5py.org)'

##### Papers

'[Wepy: A Flexible Software Framework for Simulating Rare Events with Weighted Ensemble Resampling](https://pubs.acs.org/doi/10.1021/acsomega.0c03892)' Lotz, S., & Dickson, A., ACS Omega, 2020

'[Umbrella Sampling](http://jeti.uni-freiburg.de/studenten_seminar/term_paper_WS_16_17/Kaestner11.pdf)'  Kastner, J., WIREs Comput Mol Sci, 2011

Grossfield, A, \WHAM: an implementation of the weighted histogram analysis method", http://membrane.urmc.rochester.edu/content/wham/, version 2.0.10.1  

OpenMMTools, https://github.com/choderalab/openmmtools
