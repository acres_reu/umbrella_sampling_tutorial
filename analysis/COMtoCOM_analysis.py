import sys
import numpy as np
import pickle

from matplotlib import pyplot as plt
import mdtraj as mdj
from wepy.hdf5 import WepyHDF5

pdb_path = '../inputs/dummy_top.pdb'

run_idx = 0
n_frames = int(sys.argv[1])
run = int(sys.argv[2])

files = []
for i in range(0, n_frames):
    files.append([f'../outputs/eps5/windows{n_frames}/run{run}/US_frame{i}.wepy.h5'])


def get_COMs(positions):

    small_pos = np.array([positions[0], positions[1]])

    return small_pos

# analysis of data in files
def min3(a,b,c):
    if a < b:
        if a < c:
            return a
        else:
            return c
    else:
        if b < c:
            return b
        else:
            return c
        

def vec_dist(d1,d2):
    tmp = d1-d2
    for i,t in enumerate(tmp):
        tmp[i] = min3(abs(t),abs(t-box),abs(t+box))

    if np.sqrt(np.sum(np.square(tmp))) > 8:
        print("d1 is",d1)
        print("d2 is",d2)
    return np.sqrt(np.sum(np.square(tmp)))

def uncentered_pos(hdf5):
    return np.array(wepy_h5.h5[f'runs/0/trajectories/0/positions'])

if __name__=='__main__':

    # this block opens 1 of your h5 files and pulls out consistant parameters
    wepy_h5 = WepyHDF5(f'../outputs/eps5/windows{n_frames}/run{run}/US_frame0.wepy.h5', mode='r') 
    wepy_h5.open()
    n_cycles = wepy_h5.num_run_cycles(0) # same for all runs
    n_atoms = wepy_h5.num_atoms          # same for all runs

    box = 4.31       # nm, same for all runs
    n_part = n_atoms # n_atoms in system, needed for output
    n_dim = 3        # num dimentions of positions, (3: x,y,z)
    n_walk = 1       # number of walkers in each simulation
    distances = np.zeros((len(files), n_cycles))

    for index_f, value_f in enumerate(files): # this iterates through all of your hdf5 files

        wepy_h5 = WepyHDF5(value_f[0], mode='r') 
        wepy_h5.open()

        uncentered_positions = uncentered_pos(wepy_h5) # pulls all positions from h5

        # Get the COM-to-COM per cycle
        print(f'Getting COM-to-COM distances for file {index_f+1}')

        for index_ucp, value_ucp in enumerate(uncentered_positions): # calculates the COM-to-COM distance for every cycle 
            small_pos = get_COMs(value_ucp)                          # in this file (every cycle of this window run)
            tmp = vec_dist(small_pos[0], small_pos[1])
            distances[index_f][index_ucp] = tmp

        wepy_h5.close()
        
    n_bins = 10
    binned = np.zeros((len(files), n_bins)) # stores histogram info for all your data
    x_vals = np.zeros((len(files), n_bins)) # stores histogram info for all your data
    for index,value in enumerate(distances): # iterates through CTC dists for every run, generates histograms
        
        binned_vals, bins = np.histogram(value, bins=n_bins, range=(np.min(value), np.max(value)))
        binned[index] = binned_vals
        xvals = np.linspace(min(value), max(value), n_bins)
        x_vals[index] = xvals

    # these 3 files contain arrays that have your histograms for all 75 or 113 of your runs!
    np.savetxt(f'distances_eps5_w{n_frames}_run{run}.txt', distances)
    np.savetxt(f'xvals_eps5_w{n_frames}_run{run}.txt', x_vals)
    np.savetxt(f'binned_eps5_w{n_frames}_run{run}.txt', binned)

