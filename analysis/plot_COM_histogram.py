import numpy as np
import sys
import matplotlib.pyplot as plt

n_frames = int(sys.argv[1])
run = int(sys.argv[2])

binned = np.loadtxt(f'binned_eps5_w{n_frames}_run{run}.txt') 
xvals = np.loadtxt(f'xvals_eps5_w{n_frames}_run{run}.txt')

if n_frames == 75:
    d0_numbers = [0.315 + 15*i*0.0015 for i in range (75)]
if n_frames == 113:
    d0_numbers = [0.315 + 10*i*0.0015 for i in range (113)]

for i in range(len(binned)):
    tmp = binned[i]/np.sum(binned[i])
    plt.plot(xvals[i], tmp, label = f'frame {i+1}')

plt.ylabel('Probability')
plt.xlabel('COM-to-COM (nm)')
plt.title(f'COM-to-COM Distribution for {n_frames} windows')
# plt.legend()
plt.show()
